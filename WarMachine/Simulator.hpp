#pragma once

#include <queue>
#include <vector>
#include <memory>
#include "Event.hpp"
#include "Client.hpp"

enum class SimState
{
	Error,
	Inactive,
	Completed,
	Active,
	Waiting
};

class Simulator
{
public:

	Simulator();
	~Simulator();
	
	void Update(); //update one frame (Flushes the event queue)
	void Enqueue(const Event& Event); //pass an event to the simulator

	inline bool IsGameRunning() const { return state >= SimState::Active; }
	
protected:
	SimState state;
	std::queue<Event> evq; ///Internal event queue for incoming messages
	std::vector<Client*> clients; //all of the clients

	void SendClients(const Event& Ev); //send an event to all clients
};

//sent from simulator on a successful connect, sends nullptr if rejected
struct ConnectInfo
{
	Simulator* sim;
	int numClients; //excludes 'me'

};