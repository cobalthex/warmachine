#pragma once

#include "Entity.hpp"
#include <cassert>

enum EventType
{
	Ev_Unknown,
	Ev_Connect,
	Ev_Disconnect,

	Ev_State, //get the state of the game
};

class Client;
class Event
{
public:
	Event(EventType Type, void* const Data, Client* const Client = nullptr, Entity* const Sender = nullptr, Entity* const Receiver = nullptr);
	~Event() { }

	inline EventType GetType() const { return type; }
	inline void* const GetData() const { return data; }
	inline Client* const GetClient() const { return client; }
	inline Entity* const GetSender() const { return sender; }
	inline Entity* const GetReceiver() const { return receiver; }

private:
	EventType type;
	void* data;
	Client* client;
	Entity* sender;
	Entity* receiver;
};

