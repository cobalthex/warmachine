#pragma once

#include <vector>
#include "Cartes.hpp"
#include "Entity.hpp"

//A single layer of a quad tree
class QuadTree
{
public:
	QuadTree(const Rect& Bounds);
	~QuadTree(); //delets all children
	
	bool Insert(Entity* const Ent);
	
	std::vector<Entity*> Query(const Rect& Region) const;

	inline Rect GetBounds() const { return bounds; }
	void Resize(const Rect& NewBounds);

protected:
	// nw | ne
	// ---+---
	// sw | se

	QuadTree* nw;
	QuadTree* ne;
	QuadTree* sw;
	QuadTree* se;
	std::vector<Entity*> ents; ///the nodes at this level

	bool Subdivide();
	inline int GetDepth() { return depth; }

private:
	QuadTree(const Rect& Bounds, int Depth);
	Rect bounds;
	int depth;
	
	const static int MAX_DEPTH; //maximum depth for creating children
};