#include "Simulator.hpp"

Simulator::Simulator()
{
	evq = std::queue<Event>();
	state = SimState::Active;
}


Simulator::~Simulator()
{
}

void Simulator::Update()
{
	while (!evq.empty())
	{
		Event ev = evq.front();
		Client* evc = ev.GetClient();

		switch (ev.GetType())
		{
		case Ev_Connect:
			if (evc != nullptr)
			{
				clients.push_back(evc);

				ConnectInfo* connInfo = new ConnectInfo;
				connInfo->numClients = clients.size() - 1;
				connInfo->sim = this;

				evc->Enqueue(Event(Ev_Connect, connInfo, evc));
			}
			break;
		case Ev_Disconnect:
			if (evc != nullptr)
			{
				for (unsigned i = 0; i < clients.size(); i++)
					if (clients[i] == evc)
					{
						clients.erase(clients.begin() + i);
						evc->Enqueue(Event(Ev_Disconnect, nullptr, evc));
						break;
					}
			}
			break;
		case Ev_State:
			if (evc != nullptr)
				evc->Enqueue(Event(Ev_State, &state, evc)); 
			break;
		}
		evq.pop();

	}
}

void Simulator::Enqueue(const Event& Ev)
{
	evq.push(Ev);
}

void Simulator::SendClients(const Event& Ev)
{
	for (unsigned i = 0; i < clients.size(); i++)
		clients[i]->Enqueue(Ev);
}