#include "Cartes.hpp"

float Point::Length() const
{
	return (float)sqrt((x * x) + (y * y));
}
int Point::LengthSq() const
{
	return (x * x) + (y * y);
}
float Point::Distance(const Point& A, const Point& B)
{
	return (A - B).Length();
}
int Point::DistanceSq(const Point& A, const Point& B)
{
	return (A - B).LengthSq();
}

Point Point::operator+(const Point& Pt) const
{
	return Point(x + Pt.x, y + Pt.y);
}
Point Point::operator-(const Point& Pt) const
{
	return Point(x + Pt.x, y + Pt.y);
}
Point Point::operator*(int Magnitude) const
{
	return Point(x * Magnitude, y * Magnitude);
}
Point Point::operator/(int Magnitude) const
{
	return Point(x / Magnitude, y / Magnitude);
}