#pragma once

#include <queue>
#include "Event.hpp"

class Simulator;
class Client
{
public:
	Client();
	~Client();

	virtual void Update() = 0; //update one frame (Flushes the event queue)
	void Enqueue(const Event& Event);
	inline bool IsConnected() const { return (sim != nullptr); }

protected:
	std::queue<Event> evq;
	Simulator* sim;
};

