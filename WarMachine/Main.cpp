#include "Main.h"
#include "Player.hpp"
#include "Simulator.hpp"

int main(int ac, char* av[], char* env)
{
	Player* p1 = new Player;
	Simulator* sim = new Simulator;
	sim->Enqueue(Event(Ev_Connect, nullptr, p1));
	sim->Enqueue(Event(Ev_State, nullptr, p1));

	int ctr = 0;
	while (sim->IsGameRunning() && ctr < 3000000)
	{
		sim->Update();
		p1->Update();
		ctr++;
	}
	sim->Enqueue(Event(Ev_Disconnect, nullptr, p1));
	sim->Update();
	p1->Update();

	//temporary
	system("pause");

	return 0;
}