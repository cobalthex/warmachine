#pragma once

#include "Cartes.hpp"

class Entity
{ 
public:
	Entity(void);
	virtual ~Entity(void);

	inline Point GetPos() const { return position; }
	inline Point GetSize() const { return size; }
	Rect GetBounds() const; //Get the boundaries of the entity. Note, the position is centered inside the size

protected:
	Point position;
	Point size;

	friend class Simulator;
};

