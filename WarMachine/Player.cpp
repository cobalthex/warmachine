#include "Player.hpp"
#include "Simulator.hpp"
#include <iostream>
using namespace std;

Player::Player()
{
}


Player::~Player()
{
}

void Player::Update()
{
	while (!evq.empty())
	{
		Event ev = evq.front();

		switch (ev.GetType())
		{
		case Ev_Connect:
			{
				ConnectInfo* connInfo = (ConnectInfo*)ev.GetData();
				if (connInfo != nullptr)
				{
					sim = connInfo->sim;
					clog << "Connected to simulator. There are " << connInfo->numClients << " other clients connected.\n";
					delete connInfo;
				}
				else
					clog << "Error connecting to simulator\n";
				break;
			}
		case Ev_Disconnect:
			clog << "Disconnected.\n";
			break;
		case Ev_State:
			clog << "Game State: " << *(int*)ev.GetData() << endl;
			break;
		}
		evq.pop();

	}
}
