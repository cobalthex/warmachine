#include "QuadTree.hpp"

const int QuadTree::MAX_DEPTH = 4;

QuadTree::QuadTree(const Rect& Bounds, int Depth)
{
	bounds = Bounds;
	depth = Depth;

	ents = std::vector<Entity*>();

	ne = nullptr;
	se = nullptr;
	nw = nullptr;
	sw = nullptr;
}
QuadTree::QuadTree(const Rect& Bounds)
{
	QuadTree(Bounds, 0);
}

QuadTree::~QuadTree()
{
	if (nw != nullptr)
	{
		delete nw;
		nw = nullptr;
	}
	if (ne != nullptr)
	{
		delete ne;
		ne = nullptr;
	}
	if (sw != nullptr)
	{
		delete sw;
		sw = nullptr;
	}
	if (se != nullptr)
	{
		delete se;
		se = nullptr;
	}
}

bool QuadTree::Insert(Entity* const Node)
{
	//Ignore objects which are outside
	if (!bounds.Contains(Node->GetPos()))
		return false;

	Point sz = Node->GetSize();
	//only insert if larger than half the size of the node (otherwise, subdivide)
	if ((depth + 1) < MAX_DEPTH && (sz.x >= bounds.w / 2 || sz.y > bounds.h / 2))
	{
		ents.push_back(Node);
		return true;
	}

	//Otherwise, we need to subdivide then add the point to whichever node will accept it
	if (nw == nullptr)
		Subdivide();

	if (nw->Insert(Node) || ne->Insert(Node) || sw->Insert(Node) || se->Insert(Node))
		return true;

	//Otherwise, the point cannot be inserted for some unknown reason (which should never happen)
	return false;
}

bool QuadTree::Subdivide()
{
	if (ne != nullptr || nw != nullptr || sw != nullptr || se != nullptr)
		return false;

	ne = new QuadTree(Rect(bounds.x + (bounds.w >> 1), bounds.y, bounds.w >> 1, bounds.h >> 1), depth + 1);
	se = new QuadTree(Rect(bounds.x + (bounds.w >> 1), bounds.y + (bounds.h >> 1), bounds.w >> 1, bounds.h >> 1), depth + 1);
	nw = new QuadTree(Rect(bounds.x, bounds.y, bounds.w >> 1, bounds.h >> 1), depth + 1);
	sw = new QuadTree(Rect(bounds.x, bounds.y + (bounds.h >> 1), bounds.w >> 1, bounds.h >> 1), depth + 1);

	return true;
}

void QuadTree::Resize(const Rect& NewBounds)
{
	bounds = NewBounds;
	if (ne != nullptr) ne->Resize(Rect(bounds.x + (bounds.w >> 1), bounds.y, bounds.w >> 1, bounds.h >> 1));
	if (nw != nullptr) nw->Resize(Rect(bounds.x + (bounds.w >> 1), bounds.y + (bounds.h >> 1), bounds.w >> 1, bounds.h >> 1));
	if (se != nullptr) se->Resize(Rect(bounds.x, bounds.y, bounds.w >> 1, bounds.h >> 1));
	if (sw != nullptr) sw->Resize(Rect(bounds.x, bounds.y + (bounds.h >> 1), bounds.w >> 1, bounds.h >> 1));
}

inline void AddGroup(std::vector<Entity*>& Vtr, std::vector<Entity*>& ToAdd)
{
	Vtr.insert(Vtr.end(), ToAdd.begin(), ToAdd.end());
}

std::vector<Entity*> QuadTree::Query(const Rect& Range) const
{
	// Prepare an array of results
	std::vector<Entity*> pointsInRange;

	// Automatically abort if the range does not collide with this quad
	if (!bounds.Intersects(Range))
		return pointsInRange; // empty list

	// Check objects at this quad level
	for (unsigned i = 0; i < ents.size(); i++)
	{
		if (Range.Contains(ents[i]->GetPos()))
			pointsInRange.push_back(ents[i]);
	}

	// Terminate here if there are no children
	if (nw == nullptr)
		return pointsInRange;

	// Otherwise, add the points from the children
	AddGroup(pointsInRange, nw->Query(Range));
	AddGroup(pointsInRange, ne->Query(Range));
	AddGroup(pointsInRange, sw->Query(Range));
	AddGroup(pointsInRange, se->Query(Range));

	return pointsInRange;
}