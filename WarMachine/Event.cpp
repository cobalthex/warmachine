#include "Event.hpp"

Event::Event(EventType Type, void* const Data, Client* const Client, Entity* const Sender, Entity* const Receiver)
	: type(Type), data(Data), client(Client), sender(Sender), receiver(Receiver)
{
}