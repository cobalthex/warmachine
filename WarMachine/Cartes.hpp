#pragma once

//2D cartesian helper classes
#include <cmath>

///2D point
struct Point
{
	int x, y;
	Point() : x(0), y(0) { }
	Point(int Mag) : x(Mag), y(Mag) { }
	Point(int X, int Y) : x(X), y(Y) { }

	float Length() const;
	int LengthSq() const;
	static float Distance(const Point& A, const Point& B);
	static int DistanceSq(const Point& A, const Point& B);

	Point operator+(const Point& Pt) const;
	Point operator-(const Point& Pt) const;
	Point operator*(int Magnitude) const;
	Point operator/(int Magnitude) const;
};

///2D Rectangle
struct Rect
{
	int x, y, w, h;
	Rect() : x(0), y(0), w(0), h(0) { }
	Rect(int X, int Y, int W, int H) : x(X), y(Y), w(W), h(H) { }

	inline bool Contains(const Point& Pt) const { return (Pt.x >= x && Pt.y >= y && Pt.x < x + w && Pt.y < y + h); }
	inline bool Intersects(const Rect& Rct) const { return !(Rct.x > (x + w) || (Rct.x + Rct.w) < x || Rct.y > (y + h) || (Rct.y + Rct.h) < y); }
};
